<?php

use DarujmeDeps\DI\Definition\Helper\CreateDefinitionHelper;
use DarujmeDeps\Wpify\CustomFields\CustomFields;
use DarujmeDeps\Wpify\PluginUtils\PluginUtils;
use DarujmeDeps\Wpify\Template\WordPressTemplate;

return array(
	CustomFields::class      => ( new CreateDefinitionHelper() )
		->constructor( plugins_url( 'deps/wpify/custom-fields', __FILE__ ) ),
	WordPressTemplate::class => ( new CreateDefinitionHelper() )
		->constructor( array( __DIR__ . '/templates' ), 'darujme' ),
	PluginUtils::class       => ( new CreateDefinitionHelper() )
		->constructor( __DIR__ . '/darujme.php' ),
);
