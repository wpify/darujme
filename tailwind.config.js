module.exports = {
	content: [
		'./src/**/*.php',
		'./assets/**/*.jsx',

	],
	safelist: [
		'swiper-button-disabled',
		'site-header--sticky',
		'page-numbers',
		'animation',
		'animation--visible',
		'tippy-box',
		'tippy-arrow'
	],
	variants: {},
	plugins: [],
	corePlugins: {
		container: false,
	},
};
