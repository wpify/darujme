<?php
/*
 * Plugin Name:       Darujme
 * Description:       Integration with Darujme.cz
 * Version:           WPORG_VERSION
 * Requires PHP:      7.3.0
 * Requires at least: 5.4.0
 * Author:            WPify
 * Author URI:        https://www.wpify.io/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       darujme
 * Domain Path:       /languages
*/

use Darujme\Plugin;
use DarujmeDeps\DI\Container;
use DarujmeDeps\DI\ContainerBuilder;

if ( ! defined( 'DARUJME_MIN_PHP_VERSION' ) ) {
	define( 'DARUJME_MIN_PHP_VERSION', '7.3.0' );
}

/**
 * @return Plugin
 * @throws Exception
 */
function darujme(): Plugin {
	return darujme_container()->get( Plugin::class );
}

/**
 * @return Container
 * @throws Exception
 */
function darujme_container(): Container {
	static $container;

	if ( empty( $container ) ) {
		$is_production    = ! WP_DEBUG;
		$file_data        = get_file_data( __FILE__, array( 'version' => 'Version' ) );
		$definition       = require_once __DIR__ . '/config.php';
		$containerBuilder = new ContainerBuilder();
		$containerBuilder->addDefinitions( $definition );

		if ( $is_production ) {
			$containerBuilder->enableCompilation( WP_CONTENT_DIR . '/cache/' . dirname( plugin_basename( __FILE__ ) ) . '/' . $file_data['version'], 'DarujmeCompiledContainer' );
		}

		$container = $containerBuilder->build();
	}

	return $container;
}

function darujme_activate( $network_wide ) {
	darujme()->activate( $network_wide );
}

function darujme_deactivate( $network_wide ) {
	darujme()->deactivate( $network_wide );
}

function darujme_uninstall() {
	darujme()->uninstall();
}

function darujme_php_upgrade_notice() {
	$info = get_plugin_data( __FILE__ );

	echo sprintf(
		__( '<div class="error notice"><p>Opps! %s requires a minimum PHP version of %s. Your current version is: %s. Please contact your host to upgrade.</p></div>', 'darujme' ),
		$info['Name'],
		DARUJME_MIN_PHP_VERSION,
		PHP_VERSION
	);
}

function darujme_php_vendor_missing() {
	$info = get_plugin_data( __FILE__ );

	echo sprintf(
		__( '<div class="error notice"><p>Opps! %s is corrupted it seems, please re-install the plugin.</p></div>', 'darujme' ),
		$info['Name']
	);
}

add_action( 'plugins_loaded', 'darujme_load_textdomain' );
function darujme_load_textdomain() {
	load_plugin_textdomain( 'darujme', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}

if ( version_compare( PHP_VERSION, DARUJME_MIN_PHP_VERSION ) < 0 ) {
	add_action( 'admin_notices', 'darujme_php_upgrade_notice' );
} else {
	$deps_loaded   = false;
	$vendor_loaded = false;

	$deps = array_filter( array(
		__DIR__ . '/deps/scoper-autoload.php',
		__DIR__ . '/deps/autoload.php'
	), function ( $path ) {
		return file_exists( $path );
	} );

	foreach ( $deps as $dep ) {
		include_once $dep;
		$deps_loaded = true;
	}

	if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
		include_once __DIR__ . '/vendor/autoload.php';
		$vendor_loaded = true;
	}

	if ( $deps_loaded && $vendor_loaded ) {
		add_action( 'plugins_loaded', 'darujme', 11 );
		register_activation_hook( __FILE__, 'darujme_activate' );
		register_deactivation_hook( __FILE__, 'darujme_deactivate' );
		register_uninstall_hook( __FILE__, 'darujme_uninstall' );
	} else {
		add_action( 'admin_notices', 'darujme_php_vendor_missing' );
	}
}
