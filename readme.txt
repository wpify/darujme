=== Darujme ===
Contributors: wpify, vasikgreif, mejta
Tags: Darujme, Donation
Requires at least: 5.5
Tested up to: 6.0
Requires PHP: 7.4
Stable tag: WPORG_VERSION
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A free plugin for Darujme.cz and WordPress integration.

== Description ==

A free plugin for Darujme.cz and WordPress integration. With this plugin you can easily add Darujme.cz widget to your site, use custom donation form, and easily build custom confirmation pages.

## Features

The plugin adds 5 Gutenberg blocks and shortcodes for those not using Gutenberg:

### Custom form

* Add custom donation form to your site
* Block: Darujme.cz - Custom form, Shortcode: [darujme_form]

### Darujme widget

* Easily embed Darujme.cz widget to your site
* Block: Darujme.cz - Widget, Shortcode: [darujme_widget token="the-widget-token"]

### Pledges list

* Display the list of pledges on your site
* Block: Darujme.cz - Pledges list, Shortcode: [darujme_pledges]

### Confirmation

* Displays the table with donation data
* Block: Darujme.cz - Confirmation, shortcode: [darujme_confirmation]

### QR Code

* Displays the QR code for the donation payment
* Block: Darujme.cz - QR COde, Shortcode: [darujme_qr_code]


== Installation ==

1. Upload `darujme` folder to the `/wp-content/plugins/` directory or install the plugin from the WordPress plugin repository.
2. Activate the Darujme plugin through the "Plugins" menu in WordPress.
3. Go to the administration area > Settings > Darujme.
4. Setup.
5. Add the blocks or shortcode to your pages.
6. To use custom confirmation page, create a page with Confirmation block or the shortcode [darujme_confirmation], and refer to Settings to see the return URL to set in the Darujme.cz project settings.

== Frequently Asked Questions ==

= Do you have documentation for the plugin? =

Not yet, if you run into issues, please consult Darujme.cz support for help.

= Why did you create this plugin? =

This plugin was created to simplify the integration of Darujme.cz donation forms into your WordPress site. The main motivation was to support the amazing Darujme.cz project and the non-profit organizations that use it.

= Why do you support WordPress 5.x only? =

We take advantage of the new WP features, and we strive to use modern development practices, which was not possible in the previous versions of WordPress.

= Why do you support PHP 7.4 and higher only? =

We support only actively supported versions to be sure, that our code is secure from the bottom up. It's also essential to have the PHP version regularly updated, co you can be sure that your website is safe and fast.

= I found a bug, what should I do? =

Drop us a message in the support section.

= Who is behind the plugin? =

This plugin is brought to you by Václav Greif and Daniel Mejta, the WordPress and WooCommerce experts at [wpify.io](https://wpify.io).

== Changelog ==
= 1.1.6 =
* Translations

= 1.1.5 =
* Remove monthsRecurring for frequency once

= 1.1.4 =
* Add formatted currency

= 1.1.3 =
* Add default to number of recurring months

= 1.1.2 =
* Fix custom fields positions

= 1.1.1 =
* Add custom recurring months for recurring payments

= 1.1.0 =
* Add Custom Fields Support in Custom Forms

= 1.0.1 =
* Fix translations

= 1.0.0 =
* Initial version
