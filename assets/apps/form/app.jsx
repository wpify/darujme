import React, {useState, useEffect} from 'react';
import {useForm} from "react-hook-form";
import {addQueryArgs} from "@wordpress/url";
import classNames from 'classnames';
import {__} from "@wordpress/i18n";
import TextInput from "./TextInput";
import {Transition} from '@headlessui/react'
import CustomFields from "./CustomFields";

const App = () => {
	const settings = window.darujme;

	const {register, setValue, handleSubmit, watch, formState: {errors}} = useForm();
	const [customAmountDisplayed, setCustomAmountDisplayed] = useState(false);
	const [personalDetailsDisplayed, setPersonalDetailsDisplayed] = useState(false);
	const [showNumberOfRecurringMonths, setShowNumberOfRecurringMonths] = useState(false);
	const [recurring, setRecurring] = useState(null);
	const amount = watch("amount");
	const frequency = watch("frequency");
	const paymentMethod = watch("paymentMethod");
	const currency = settings.currency;
	const formattedCurrency = currency.replace('CZK', 'Kč').replace('EUR', '€');

	const customFields = window.darujme.custom_fields ? window.darujme.custom_fields : [];
	const customFieldsToggleAddress = customFields.filter(field => field?.toggle === 'address');
	const customFieldsAddressToggled = customFieldsToggleAddress.map(field => {
		return watch(`custom[${field.darujme_api_id}]`);
	}).filter(item => item === true).length;

	const hasCertificate = watch("wantDonationCertificate") || !!customFieldsAddressToggled;

	useEffect(() => {
		if (recurring !== null) {
			setValue('frequency', recurring ? 'monthly' : 'once');
		}

		return () => {

		};
	}, [recurring]);

	useEffect(() => {
		if (amount && frequency) {
			setPersonalDetailsDisplayed(true);
		}
		return () => {

		};
	}, [amount, frequency]);

	const onSubmit = data => {
		data.wantDonationCertificate = data.wantDonationCertificate ? 1 : 0;
		data.monthsRecurring = showNumberOfRecurringMonths && data.recurringMonths ? data.recurringMonths : '-2';
		if ('once' === frequency) {
			delete data['monthsRecurring'];
		}
		window.location.href = addQueryArgs('https://www.darujme.cz/darovat/', data);
	};

	return <>
		<div className="w-full">
			<form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit(onSubmit)}>
				<div className="mb-4">
					{!customAmountDisplayed ? <>
						<label className="block text-sm font-bold mb-2" htmlFor="amount">
							{__('Choose amount', 'darujme')}
						</label>
						{settings.default_amounts.map(item => <>
							<button type="button"
									onClick={() => setValue('amount', item.amount)}
									className={classNames(
										"btn button shadow text-black font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline mr-2",
										{'bg-black text-white': item.amount === amount}
									)}>{item.amount}
							</button>
						</>)}
						<button type="button" onClick={() => setCustomAmountDisplayed(!customAmountDisplayed)}
								className="darujme__button btn button shadow text-black font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline mr-2">
							{__('Custom amount', 'darujme')}
						</button>
					</> : <>
						<div className="mb-4">
							<label className="block text-sm font-bold mb-2" htmlFor="custom-amount">
								{__('Enter amount', 'darujme')}
							</label>
							<input
								onChange={(e) => setValue('amount', e.target.value)}
								className="shadow appearance-none  border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
								id="custom-amount" type="number" placeholder={__('Enter the amount', 'darujme')}/>
						</div>
					</>}
				</div>
				<div className="darujme__frequency inline-flex rounded-md shadow-sm mb-4" role="group">
					<button type="button" onClick={() => setRecurring(true)}
							className={
								classNames(
									"darujme__button darujme__button--group py-2 px-4 text-sm font-medium text-black bg-white rounded-l-lg border border-gray-200",
									{'bg-black text-white': recurring}
								)
							}>
						{__('Monthly', 'darujme')}
					</button>
					<button type="button"
							onClick={() => setRecurring(false)}
							className={
								classNames(
									"darujme__button darujme__button--group py-2 px-4 text-sm font-medium text-black bg-white border-t border-b border-gray-200 rounded-r-lg",
									{'bg-black text-white': !recurring && recurring !== null}
								)
							}>
						{__('One time', 'darujme')}
					</button>
				</div>
				<Transition
					show={personalDetailsDisplayed}
					enter="transition-opacity duration-500"
					enterFrom="opacity-0"
					enterTo="opacity-100"
					leave="transition-opacity duration-500"
					leaveFrom="opacity-100"
					leaveTo="opacity-0"
				>
					<div className="">
						<div dangerouslySetInnerHTML={{__html: settings.content_before_personal_details}}/>
						<div className="columns-2 darujme__personal">

							<div className="mb-6">
								<TextInput
									label={__('First name', 'darujme')}
									name='firstName'
									placeholder={__('Enter your first name', 'darujme')}
									register={register}
									required={true}
								/>
							</div>
							<div className="mb-6">
								<TextInput
									label={__('Last name', 'darujme')}
									name='lastName'
									placeholder={__('Enter your last name', 'darujme')}
									register={register}
									required={true}
								/>
							</div>
						</div>
						<div className="columns-2">
							<div className="mb-6">
								<TextInput
									label={__('Phone', 'darujme')}
									name='phone'
									placeholder={__('Enter your phone', 'darujme')}
									register={register}
									required={true}
								/>
							</div>
							<div className="mb-6">
								<TextInput
									label={__('E-mail', 'darujme')}
									name='email'
									placeholder={__('Enter your e-mail', 'darujme')}
									register={register}
									required={true}
									type={'email'}
								/>
							</div>
						</div>

						<div dangerouslySetInnerHTML={{__html: settings.content_after_personal_details}}/>
						<CustomFields
							customFields={customFields}
							position='after_personal_details'
							register={register}
						/>
						<div className="mb-6">
							<label className="md:w-2/3 block text-gray-500 font-bold">
								<input
									className="mr-2 leading-tight"
									name="wantDonationCertificate"
									type="checkbox" {...register("wantDonationCertificate")}/>
								<span
									className="text-sm">{__('I want to receive donor confirmation', 'darujme')}</span></label>
						</div>
					</div>
				</Transition>

				{
					hasCertificate && <div className="">
						<div dangerouslySetInnerHTML={{__html: settings.content_before_certificate_details}}/>
						<div className="columns-3">
							<div className="mb-6">
								<TextInput
									label={__('Street', 'darujme')}
									name='street'
									placeholder={__('Street', 'darujme')}
									register={register}
									required={hasCertificate}
								/>
							</div>
							<div className="mb-6">
								<TextInput
									label={__('City', 'darujme')}
									name='city'
									placeholder={__('City', 'darujme')}
									register={register}
									required={hasCertificate}
								/>
							</div>
							<div className="mb-6">
								<TextInput
									label={__('Postcode', 'darujme')}
									name='postcode'
									placeholder={__('Postcode', 'darujme')}
									register={register}
									required={hasCertificate}
								/>
							</div>
						</div>

						<div dangerouslySetInnerHTML={{__html: settings.content_after_certificate_details}}/>

					</div>
				}

				<CustomFields
					customFields={customFields}
					position='after_address'
					register={register}
				/>
				{
					personalDetailsDisplayed && <>
						<div dangerouslySetInnerHTML={{__html: settings.content_before_payment_methods}}/>
						<h2>{__('Payment method', 'darujme')}</h2>
						<div className="inline-flex rounded-md shadow-sm mb-4" role="group">
							<button type="button" onClick={() => setValue('paymentMethod', 'proxypay_charge')}
									className={
										classNames(
											"py-2 px-4 text-sm font-medium text-black bg-white rounded-l-lg border border-gray-200",
											{'bg-black text-white': paymentMethod === 'proxypay_charge'}
										)
									}>
								{__('Credit card', 'darujme')}
							</button>
							{
								!recurring && <button type="button"
													  onClick={() => setValue('paymentMethod', 'payu_transfer')}
													  className={
														  classNames("py-2 px-4 text-sm font-medium text-black bg-white border-t border-b border-gray-200",
															  {'bg-black text-white': paymentMethod === 'payu_transfer'}
														  )

													  }>
									{__('PayU', 'darujme')}
								</button>
							}
							<button type="button"
									onClick={() => setValue('paymentMethod', 'funds_transfer')}
									className={
										classNames(
											"py-2 px-4 text-sm font-medium text-black bg-white border-t border-b border-gray-200 rounded-r-lg",
											{'bg-black text-white': paymentMethod === 'funds_transfer'}
										)
									}>
								{__('Bank transfer', 'darujme')}
							</button>
						</div>
						{recurring && <div>
							{!showNumberOfRecurringMonths && <button type="button"
																	 onClick={() => setShowNumberOfRecurringMonths(true)}
																	 className={
																		 classNames(
																			 "py-2 px-4 text-sm mb-4 font-medium text-black bg-white border-t border-b border-gray-200 rounded-r-lg",
																			 {'bg-black text-white': paymentMethod === 'funds_transfer'}
																		 )
																	 }>
								{__('Limit months for recurring payment', 'darujme')}
							</button>}
							{
								showNumberOfRecurringMonths && <TextInput
									label={__('Enter the number of months for recurring payments', 'darujme')}
									name='recurringMonths'
									register={register}
									required={recurring}
									defaultValue="12"
									min={2}
									type="number"
								/>
							}

						</div>}

						<div dangerouslySetInnerHTML={{__html: settings.content_after_payment_methods}}/>

						<CustomFields
							customFields={customFields}
							position='before_summary'
							register={register}
						/>

						<div className="shadow-md p-5 text-center">
							<h3>{__('Summary', 'darujme')}</h3>
							<div>
								<strong>{amount} {formattedCurrency}</strong> / {recurring ? __('monthly', 'darujme') : __('one-time', 'darujme')}
							</div>
						</div>
						<div dangerouslySetInnerHTML={{__html: settings.content_before_submit_button}}/>
						<button
							className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline mt-4"
							type="submit">
							{recurring ? __('Donate monthly', 'darujme') : __('Donate one time', 'darujme')}
						< /button>
						<div dangerouslySetInnerHTML={{__html: settings.content_after_submit_button}}/>
					</>
				}

				<input type="hidden" {...register("paymentMethod")} defaultValue='proxypay_charge'/>
				<input type="hidden" {...register("currency")} value={currency}/>
				<input type="hidden" {...register("amount")}/>
				<input type="hidden" {...register("frequency")}/>
				<input type="hidden" {...register("monthsRecurring")} value="-2"/>
				<input type="hidden" {...register("do")} value="submit"/>
				<input type="hidden" {...register("project")} value={settings.project_id}/>
			</form>
		</div>
	</>
}

document.addEventListener("DOMContentLoaded", function (event) {
	const items = document.querySelectorAll('.darujme-form');
	if (items) {
		items.forEach(item => ReactDOM.render(<App/>, item));
	}
});
