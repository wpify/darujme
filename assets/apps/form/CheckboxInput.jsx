import React from 'react';
import {__} from "@wordpress/i18n";

const CheckboxInput = ({label, name, placeholder, register, required, type = 'text'}) => {
	return <>
		<label className="md:w-2/3 block text-gray-500 font-bold">
			<input
				className="mr-2 leading-tight"
				name={name}
				type="checkbox" {...register(name)}/>
			<span
				className="text-sm">{label}</span></label>
	</>
}
export default CheckboxInput;

