import React from 'react';
import {__} from "@wordpress/i18n";

const TextInput = ({label, name, placeholder, register, required, type = 'text', defaultValue, min}) => {
	return <>
		<label className="block text-gray-700 text-sm font-bold mb-2" htmlFor={name}>{label}</label>
		<input
			min={min}
			{...register(name)}
			required={required}
			defaultValue={defaultValue}
			className="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
			id={name} type={type} placeholder={placeholder}/>
	</>
}
export default TextInput;

