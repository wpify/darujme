import TextInput from "./TextInput";
import CheckboxInput from "./CheckboxInput";

const CustomFields = ({customFields, position, register}) => {
	const fields = customFields.filter(field => field.position === position);

	if (!fields.length) {
		return '';
	}


	return <>
		{fields.map(field => {
			if (field.type === 'text') {
				return <TextInput
					label={field.label}
					register={register}
					name={`custom[${field.darujme_api_id}]`}
				/>
			} else if (field.type === 'checkbox') {
				return <CheckboxInput
					label={field.label}
					register={register}
					name={`custom[${field.darujme_api_id}]`}
				/>
			} else {
				return '';
			}


		})}
	</>;
}
export default CustomFields;
