<?php

namespace Darujme;

use DarujmeDeps\Wpify\PluginUtils\PluginUtils;

class Frontend {
	private Darujme $darujme;
	private PluginUtils $utils;

	public function __construct( Darujme $darujme, PluginUtils $utils ) {
		$this->darujme = $darujme;
		$this->utils   = $utils;
		$this->setup();
	}

	public function setup() {
		add_filter( 'the_content', [ $this, 'replace_placeholders' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'localize_scripts' ] );
	}

	public function replace_placeholders( $content ) {
		foreach ( $this->darujme->get_return_args() as $placeholder => $key ) {
			if ( ! empty( $_GET[ $key ] ) ) {
				$value = sanitize_text_field( $_GET[ $key ] );

				if ( $key === 'darujme_formatted_price' || $key === 'darujme_currency' ) {
					$value = str_replace( 'CZK', 'Kč', $value );
				}

				$content = str_replace( sprintf( '{%s}', $key ), esc_attr( $value ), $content );
			}
		}

		return $content;
	}

	public function localize_scripts() {
		wp_set_script_translations( 'darujme-form-js', 'darujme', $this->utils->get_plugin_path( 'languages' ) );
	}
}
