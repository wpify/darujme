<?php

namespace Darujme;

use Darujme\Managers\BlocksManager;
use DarujmeDeps\Wpify\Log\Tools;

final class Plugin {
	public function __construct(
		BlocksManager $blocks_manager,
		Settings $settings,
		Frontend $frontend,
		Shortcodes $shortcodes,
		Tools $tools
	) {
	}

	/**
	 * @param bool $network_wide
	 */
	public function activate( bool $network_wide ) {
	}

	/**
	 * @param bool $network_wide
	 */
	public function deactivate( bool $network_wide ) {
	}

	/**
	 *
	 */
	public function uninstall() {
	}
}
