<?php

namespace Darujme;

use Darujme\Repositories\SettingsRepository;

class Controller {
	private SettingsRepository $settings_repository;
	private Darujme $darujme;

	public function __construct( SettingsRepository $settings_repository, Darujme $darujme ) {
		$this->settings_repository = $settings_repository;
		$this->darujme             = $darujme;
	}

	public function get_confirmation() {
		$data  = [];
		$table = [];
		foreach ( $this->darujme->get_return_args() as $placeholder => $key ) {
			if ( ! empty( $_GET[ $key ] ) ) {
				$data[] = [
					'label' => $key,
					'value' => sanitize_text_field( $_GET[ $key ] )
				];

				if ( $this->settings_repository->get_option( 'confirmation_fields' ) && in_array( $key, array_column( $this->settings_repository->get_option( 'confirmation_fields' ), 'field' ), true ) ) {
					$value = sanitize_text_field( $_GET[ $key ] );

					if ( $key === 'darujme_formatted_price' || $key === 'darujme_currency' ) {
						$value = str_replace( 'CZK', 'Kč', $value );
					}

					$table[] = [
						'label' => $this->darujme->get_labels()[ $key ],
						'value' => $value
					];
				}
			}
		}

		return [
			'table' => $table,
			'data'  => $data
		];
	}

	public function get_pledges() {
		$url     = sprintf( 'https://www.darujme.cz/api/v1/organization/%s/pledges-by-filter', $this->settings_repository->get_option( 'organization_id' ) );
		$pledges = get_transient( 'darujme_donors' );

		if ( ! $pledges ) {
			$data = json_decode( wp_remote_retrieve_body( wp_remote_get(
				add_query_arg( [
					'apiId'     => $this->settings_repository->get_option( 'api_id' ),
					'apiSecret' => $this->settings_repository->get_option( 'api_secret' )
				], $url ) ) ) );
			if ( $data ) {
				set_transient( 'darujme_pledges', $data, HOUR_IN_SECONDS );
				$pledges = $data;
			}
		}

		return $pledges;
	}
}
