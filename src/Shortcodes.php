<?php

namespace Darujme;

use DarujmeDeps\Wpify\Template\WordPressTemplate;

class Shortcodes {
	private WordPressTemplate $template;
	private Controller $controller;

	public function __construct( WordPressTemplate $template, Controller $controller ) {
		$this->template   = $template;
		$this->controller = $controller;
		$this->setup();
	}

	public function setup() {
		add_shortcode( 'darujme_confirmation', array( $this, 'render_confirmation' ) );
		add_shortcode( 'darujme_form', array( $this, 'render_custom_form' ) );
		add_shortcode( 'darujme_widget', array( $this, 'render_widget' ) );
		add_shortcode( 'darujme_pledges', array( $this, 'render_pledges' ) );
		add_shortcode( 'darujme_qr_code', array( $this, 'render_qr_code' ) );
	}

	public function render_confirmation(): string {
		$data = $this->controller->get_confirmation();

		return $this->template->render( 'blocks/confirmation', null, $data );
	}

	public function render_custom_form(): string {
		return $this->template->render( 'blocks/custom-form' );
	}

	public function render_widget( $atts ): string {
		$atts = shortcode_atts( array(
			'token' => null,
		), $atts, 'darujme_widget' );

		return $this->template->render( 'blocks/widget', null, $atts );
	}

	public function render_pledges() {
		$pledges = $this->controller->get_pledges();

		return $this->template->render( 'blocks/pledges', null, [ 'pledges' => $pledges ] );
	}

	public function render_qr_code(): string {
		$data = $this->controller->get_confirmation();

		return $this->template->render( 'blocks/qr', null, $data );
	}

}
