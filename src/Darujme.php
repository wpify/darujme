<?php

namespace Darujme;

class Darujme {
	public function get_return_args() {
		return array(
			'klient'                => 'darujme_client',
			'projekt'               => 'darujme_project',
			'cislo_uctu'            => 'darujme_bank_account_number',
			'cislo_projektu'        => 'darujme_project_no',
			'nno_email'             => 'darujme_nno_email',
			'datum_transakce'       => 'darujme_transaction_date',
			'typ_transakce'         => 'darujme_transaction_type',
			'darujme_vs'            => 'darujme_vs',
			'castka'                => 'darujme_amount',
			'mena'                  => 'darujme_currency',
			'castka_s_menou'        => 'darujme_formatted_price',
			'cetnost'               => 'darujme_recurring',
			'text_potvrzeni_o_daru' => 'darujme_confirmation_text',
			'jmeno'                 => 'darujme_first_name',
			'prijmeni'              => 'darujme_last_name',
			'telefon'               => 'darujme_phone',
			'email'                 => 'darujme_email',
			'ulice'                 => 'darujme_street',
			'mesto'                 => 'darujme_city',
			'psc'                   => 'darujme_postcode',
			'locale'                => 'darujme_locale',
		);
	}

	public function get_labels() {
		return [
			'darujme_client'              => __( 'Client', 'darujme' ),
			'darujme_project'             => __( 'Project', 'darujme' ),
			'darujme_bank_account_number' => __( 'Bank account number', 'darujme' ),
			'darujme_project_no'          => __( 'Project no', 'darujme' ),
			'darujme_nno_email'           => __( 'NNO email', 'darujme' ),
			'darujme_transaction_date'    => __( 'Transaction date', 'darujme' ),
			'darujme_transaction_type'    => __( 'Transaction type', 'darujme' ),
			'darujme_vs'                  => __( 'VS', 'darujme' ),
			'darujme_amount'              => __( 'Amount', 'darujme' ),
			'darujme_currency'            => __( 'Payment currency', 'darujme' ),
			'darujme_formatted_price'     => __( 'Total', 'darujme' ),
			'darujme_recurring'           => __( 'Recurring', 'darujme' ),
			'darujme_confirmation_text'   => __( 'Confirmation text', 'darujme' ),
			'darujme_first_name'          => __( 'First name', 'darujme' ),
			'darujme_last_name'           => __( 'Last name', 'darujme' ),
			'darujme_phone'               => __( 'Phone', 'darujme' ),
			'darujme_email'               => __( 'Email', 'darujme' ),
			'darujme_street'              => __( 'Street', 'darujme' ),
			'darujme_city'                => __( 'City', 'darujme' ),
			'darujme_postcode'            => __( 'Postcode', 'darujme' ),
			'darujme_locale'              => __( 'Locale', 'darujme' ),
		];
	}
}
