<?php

namespace Darujme\Blocks;

use Darujme\Darujme;
use Darujme\Repositories\SettingsRepository;
use DarujmeDeps\Wpify\CustomFields\CustomFields;
use DarujmeDeps\Wpify\Template\WordPressTemplate;

class DarujmeWidgetBlock {
	private $wcf;
	private $template;
	private $settings_repository;
	private Darujme $darujme;

	public function __construct(
		CustomFields $wcf,
		WordPressTemplate $template,
		SettingsRepository $settings_repository,
		Darujme $darujme
	) {
		$this->wcf                 = $wcf;
		$this->template            = $template;
		$this->settings_repository = $settings_repository;
		$this->darujme             = $darujme;

		$this->setup();
	}

	public function setup() {
		$this->wcf->create_gutenberg_block( array(
			'name'            => 'darujme/widget',
			'title'           => __( 'Darujme.cz - Widget', 'darujme' ),
			'render_callback' => array( $this, 'render' ),
			'items'           => array(
				[
					'type'  => 'text',
					'id'    => 'token',
					'label' => __( 'Token', 'darujme' )
				]
			),
		) );
	}

	public function render( array $block_attributes, string $content ) {
		if ( defined( 'REST_REQUEST' ) && REST_REQUEST === true ) {
			return '<div style="background-color: #e9e9e9; text-align: center; padding: 50px 0 20px;"><h3>' . __( 'Darujme.cz - Widget', 'darujme' ) . '</h3></div>';
		}

		return $this->template->render( 'blocks/widget', null, $block_attributes );
	}
}
