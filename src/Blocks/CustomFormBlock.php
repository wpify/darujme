<?php

namespace Darujme\Blocks;

use Darujme\Repositories\SettingsRepository;
use DarujmeDeps\Wpify\Asset\AssetFactory;
use DarujmeDeps\Wpify\CustomFields\CustomFields;
use DarujmeDeps\Wpify\PluginUtils\PluginUtils;
use DarujmeDeps\Wpify\Template\WordPressTemplate;

class CustomFormBlock {
	private $wcf;
	private $template;
	private AssetFactory $asset_factory;
	private PluginUtils $utils;
	private SettingsRepository $settings_repository;

	public function __construct(
		CustomFields $wcf,
		WordPressTemplate $template,
		AssetFactory $asset_factory,
		PluginUtils $utils,
		SettingsRepository $settings_repository
	) {
		$this->wcf                 = $wcf;
		$this->template            = $template;
		$this->asset_factory       = $asset_factory;
		$this->utils               = $utils;
		$this->settings_repository = $settings_repository;
		$this->setup();
	}

	public function setup() {
		$this->asset_factory->wp_script( $this->utils->get_plugin_path( 'build/form.js' ), array(
			'handle'     => 'darujme-form-js',
			'variables'  => array(
				'darujme' => $this->settings_repository->get_options()
			),
			'do_enqueue' => '__return_false',
			'in_footer'  => true
		) );
		$this->asset_factory->wp_script( $this->utils->get_plugin_path( 'build/plugin.css' ), [
			'do_enqueue' => '__return_false',
			'handle'  => 'darujme-form-css',
		] );
		$this->wcf->create_gutenberg_block( array(
			'name'            => 'darujme/custom-form',
			'title'           => __( 'Darujme.cz - Custom form', 'darujme' ),
			'render_callback' => array( $this, 'render' ),
			'items'           => array(),
			'script'          => 'darujme-form-js',
			'style'           => 'darujme-form-css',
		) );
	}

	public function render( array $block_attributes, string $content ) {
		if ( defined( 'REST_REQUEST' ) && REST_REQUEST === true ) {
			return '<div style="background-color: #e9e9e9; text-align: center; padding: 50px 0 20px;"><h3>' . __( 'Darujme.cz - Custom form', 'darujme' ) . '</h3></div>';
		}

		return $this->template->render( 'blocks/custom-form' );
	}
}
