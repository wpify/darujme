<?php

namespace Darujme\Blocks;

use Darujme\Controller;
use Darujme\Repositories\SettingsRepository;
use DarujmeDeps\Wpify\CustomFields\CustomFields;
use DarujmeDeps\Wpify\Template\WordPressTemplate;

class DarujmePledgesBlock {
	private $wcf;
	private $template;
	private $settings_repository;
	private Controller $controller;

	public function __construct(
		CustomFields $wcf,
		WordPressTemplate $template, Controller $controller
	) {
		$this->wcf        = $wcf;
		$this->template   = $template;
		$this->controller = $controller;
		$this->setup();

	}

	public function setup() {
		$this->wcf->create_gutenberg_block( array(
			'name'            => 'darujme/pledges-list',
			'title'           => __( 'Darujme.cz - Pledges list', 'darujme' ),
			'render_callback' => array( $this, 'render' ),
			'items'           => array(),
		) );
	}

	public function render( array $block_attributes, string $content ) {
		if ( defined( 'REST_REQUEST' ) && REST_REQUEST === true ) {
			return '<div style="background-color: #e9e9e9; text-align: center; padding: 50px 0 20px;"><h3>' . __( 'Darujme.cz - Pledges list', 'darujme' ) . '</h3></div>';
		}

		$pledges = $this->controller->get_pledges();

		return $this->template->render( 'blocks/pledges', null, [ 'pledges' => $pledges ] );
	}
}
