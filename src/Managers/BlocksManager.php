<?php

namespace Darujme\Managers;

use Darujme\Blocks\ConfirmationBlock;
use Darujme\Blocks\CustomFormBlock;
use Darujme\Blocks\DarujmePledgesBlock;
use Darujme\Blocks\DarujmeWidgetBlock;

use Darujme\Blocks\QRCodeBlock;
use DarujmeDeps\Wpify\Asset\AssetFactory;
use DarujmeDeps\Wpify\PluginUtils\PluginUtils;

final class BlocksManager {
	public function __construct(
		CustomFormBlock $custom_form_block,
		DarujmePledgesBlock $pledges_block,
		ConfirmationBlock $confirmation_block,
		DarujmeWidgetBlock $darujme_widget_block,
		QRCodeBlock  $QR_code_block
	) {
		$this->setup();
	}

	public function setup() {
		add_filter( 'block_categories', array( $this, 'block_categories' ), 10, 2 );
	}

	public function block_categories( $categories, $post ) {
		$categories[] = array(
			'slug'  => 'darujme',
			'title' => __( 'Darujme', 'darujme' ),
			'icon'  => 'wordpress',
		);

		return $categories;
	}
}
