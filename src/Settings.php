<?php

namespace Darujme;

use DarujmeDeps\Wpify\CustomFields\CustomFields;

/**
 * Class Settings
 *
 * @package Wpify\Settings
 */
class Settings {
	/**
	 * @var CustomFields
	 */
	public $wcf;

	/**
	 * @var array
	 */
	public $options = array();

	/**
	 * Option key, and option page slug
	 *
	 * @var string
	 */
	const KEY = 'darujme_options';
	private Darujme $darujme;

	public function __construct( CustomFields $wcf, Darujme $darujme ) {
		$this->wcf     = $wcf;
		$this->darujme = $darujme;
		$this->setup();
	}

	public function setup() {
		$return_args = [];
		$fields      = [];
		foreach ( $this->darujme->get_return_args() as $placeholder => $key ) {
			$return_args[ $key ] = sprintf( '{%s}', $placeholder );
			$fields[]            = [
				'label' => $key,
				'value' => $key,
			];
		}


		$this->wcf->create_options_page( array(
			'parent_slug' => 'options-general.php',
			'page_title'  => __( 'Darujme Settings', 'darujme' ),
			'menu_title'  => __( 'Darujme', 'darujme' ),
			'menu_slug'   => self::KEY,
			'capability'  => 'manage_options',
			'items'       => array(
				array(
					'id'    => self::KEY,
					'type'  => 'group',
					'items' => array(
						array(
							'title' => __( 'Project ID', 'darujme' ),
							'id'    => 'project_id',
							'type'  => 'text',
						),
						array(
							'title' => __( 'API ID', 'darujme' ),
							'id'    => 'api_id',
							'type'  => 'text',
						),
						array(
							'title' => __( 'API Secret', 'darujme' ),
							'id'    => 'api_secret',
							'type'  => 'text',
						),
						array(
							'title' => __( 'Organization ID', 'darujme' ),
							'id'    => 'organization_id',
							'type'  => 'text',
						),
						array(
							'title'   => __( 'Currency', 'darujme' ),
							'id'      => 'currency',
							'type'    => 'select',
							'options' => [
								[
									'label' => 'CZK',
									'value' => 'CZK',
								],
								[
									'label' => 'EUR',
									'value' => 'EUR',
								],
								[
									'label' => 'USD',
									'value' => 'USD',
								],
								[
									'label' => 'GBP',
									'value' => 'GBP',
								]
							]
						),
						array(
							'title' => __( 'Default amounts', 'darujme' ),
							'id'    => 'default_amounts',
							'type'  => 'multi_group',
							'items' => [
								array(
									'title' => __( 'Amount', 'darujme' ),
									'id'    => 'amount',
									'type'  => 'text',
								),
							]
						),
						array(
							'title' => __( 'Confirmation table fields', 'darujme' ),
							'id'    => 'confirmation_fields',
							'type'  => 'multi_group',
							'items' => [
								array(
									'title'   => __( 'Field', 'darujme' ),
									'id'      => 'field',
									'type'    => 'select',
									'options' => $fields,
								),
							]
						),
						array(
							'title' => __( 'Content before personal details', 'darujme' ),
							'id'    => 'content_before_personal_details',
							'type'  => 'wysiwyg',
						),
						array(
							'title' => __( 'Content before certificate details', 'darujme' ),
							'id'    => 'content_before_certificate_details',
							'type'  => 'wysiwyg',
						),
						array(
							'title' => __( 'Content after certificate details', 'darujme' ),
							'id'    => 'content_after_certificate_details',
							'type'  => 'wysiwyg',
						),
						array(
							'title' => __( 'Content before payment methods', 'darujme' ),
							'id'    => 'content_before_payment_methods',
							'type'  => 'wysiwyg',
						),
						array(
							'title' => __( 'Content after payment methods', 'darujme' ),
							'id'    => 'content_after_payment_methods',
							'type'  => 'wysiwyg',
						),
						array(
							'title' => __( 'Content before submit button', 'darujme' ),
							'id'    => 'content_before_submit_button',
							'type'  => 'wysiwyg',
						),
						array(
							'title' => __( 'Content after submit button', 'darujme' ),
							'id'    => 'content_after_submit_button',
							'type'  => 'wysiwyg',
						),
						array(
							'title'   => __( 'Return URL', 'darujme' ),
							'id'      => 'return_url',
							'type'    => 'html',
							'content' => __( sprintf( 'In your project settings set the return url to your thankyou page with following query args: <code>%s</code>',
								add_query_arg( $return_args, '' )
							) ),
						),
						array(
							'title' => __( 'Custom fields', 'darujme' ),
							'id'    => 'custom_fields',
							'type'  => 'multi_group',
							'items' => [
								array(
									'title' => __( 'Title', 'darujme' ),
									'id'    => 'label',
									'type'  => 'text',
								),
								array(
									'title'   => __( 'Type', 'darujme' ),
									'id'      => 'type',
									'type'    => 'select',
									'options' => [
										[
											'label' => __( 'Text', 'darujme' ),
											'value' => 'text',
										],
										[
											'label' => __( 'Checkbox', 'darujme' ),
											'value' => 'checkbox',
										],

									]
								),
								array(
									'title' => __( 'Darujme API ID', 'darujme' ),
									'id'    => 'darujme_api_id',
									'type'  => 'text',
								),
								array(
									'title'   => __( 'Position', 'darujme' ),
									'id'      => 'position',
									'type'    => 'select',
									'options' => [
										[
											'label' => __( 'After personal details', 'darujme' ),
											'value' => 'after_personal_details',
										],
										[
											'label' => __( 'After address', 'darujme' ),
											'value' => 'after_address',
										],
										[
											'label' => __( 'Before summary', 'darujme' ),
											'value' => 'before_summary',
										],
									]
								),
								array(
									'title'   => __( 'Toggle', 'darujme' ),
									'id'      => 'toggle',
									'type'    => 'select',
									'options' => [
										[
											'label' => __( 'Address', 'darujme' ),
											'value' => 'address',
										],
									]
								),
							]
						),
					),
				),
			),
		) );
	}
}
