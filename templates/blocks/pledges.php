<?php
/**
 * @var $args array
 */
do_action( 'darujme_before_donors_list', $args );
if ( ! empty( $args['pledges'] ) ) { ?>
	<table>
		<thead>
		<tr>
			<th><?php _e( 'Date', 'darujme' ); ?></th>
			<th><?php _e( 'Name', 'darujme' ); ?></th>
			<th><?php _e( 'Amount', 'darujme' ); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ( $args['pledges']->pledges as $pledge ) { ?>
			<tr>
				<td><?php echo date( 'd.m.Y', strtotime( $pledge->pledgedAt ) ); ?></td>
				<td><?php echo sprintf( '%s %s', $pledge->donor->firstName, $pledge->donor->lastName ); ?></td>
				<td><?php echo sprintf( '%s %s', $pledge->pledgedAmount->cents / 100, $pledge->pledgedAmount->currency ); ?></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
<?php }
do_action( 'darujme_after_donors_list', $args );
