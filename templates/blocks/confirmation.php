<?php
/**
 * @var $args array
 */
do_action( 'darujme_before_confirmation_table', $args );
if ( ! empty( $args['table'] ) ) { ?>
	<table>
		<tbody>
		<?php foreach ( $args['table'] as $row ) { ?>
			<tr>
				<td><?php echo esc_html( $row['label'] ); ?></td>
				<td><?php echo esc_html( $row['value'] ); ?></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
<?php }
do_action( 'darujme_after_confirmation_table', $args );
