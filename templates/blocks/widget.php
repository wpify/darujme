<?php
/**
 * @var array $args
 */
if ( empty( $args['token'] ) ) {
	return;
}

?>
<div data-darujme-widget-token="<?php echo esc_attr( $args['token'] ); ?>">&nbsp;</div>
<script type="text/javascript">
	+function(w, d, s, u, a, b) {
		w['DarujmeObject'] = u;
		w[u] = w[u] || function () { (w[u].q = w[u].q || []).push(arguments) };
		a = d.createElement(s); b = d.getElementsByTagName(s)[0];
		a.async = 1; a.src = "https:\/\/www.darujme.cz\/assets\/scripts\/widget.js";
		b.parentNode.insertBefore(a, b);
	}(window, document, 'script', 'Darujme');
	Darujme(1, "<?php echo esc_attr( $args['token'] ); ?>", 'render', "https:\/\/www.darujme.cz\/widget?token=<?php echo esc_attr( $args['token'] ); ?>", "100%");
</script>
