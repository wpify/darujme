<?php
/**
 * @var $args array
 */
do_action( 'darujme_before_qr_code', $args );

$account_prefix = '';

$data = [];
foreach ( $args['data'] as $item ) {
	$data[ $item['label'] ] = $item['value'];
}

if ( empty( $data['darujme_bank_account_number'] ) ) {
	return;
}
$account_number = $data['darujme_bank_account_number'];

if ( str_contains( $account_number, '-' ) ) {
	$ex             = explode( '-', $account_number );
	$account_prefix = $ex[0];
	$account_number = $ex[1];
}
$code           = explode( '/', $account_number )[1];
$account_number = explode( '/', $account_number )[0];

$qr_args = [
		'accountNumber' => $account_number,
		'bankCode'      => $code,
		'amount'        => floatval( $data['darujme_amount'] ),
		'currency'      => $data['darujme_currency'],
		'vs'            => $data['darujme_vs'],
		'message'       => urlencode( sprintf( __( 'Donation - %s', 'darujme' ), $data['darujme_project'] ) ),
];
if ( $account_prefix ) {
	$qr_args['accountPrefix'] = $account_prefix;
}
$url    = add_query_arg( $qr_args, 'https://api.paylibo.com/paylibo/generator/czech/image' );
$qrCode = base64_encode( wp_remote_retrieve_body( wp_remote_get( $url ) ) );
$qrCode = "data:image/png;base64,{$qrCode}"; ?>
	<img src="<?php echo esc_attr( $qrCode ); ?>" alt="">
<?php
do_action( 'darujme_after_qr_code', $args );
